# Incentivar Frontend Test

Crie um aplicativo da web chamado IncentiveMusic usado para exibir as listas de reprodução preferidas dos clientes da incentivar. O aplicativo da web tem apenas uma página:
* Uma página que lista as listas de reprodução em destaque no Spotify de acordo com alguns critérios.

## Regras de negócio

* A página é composta por dois componentes
    * Uma lista de playlists em destaque
    * Um componente de filtro com campos de filtro de API e uma entrada de texto de pesquisa local para filtrar as listas de reprodução por "nome".
    
* O componente de filtro deve ser usado para filtrar os elementos exibidos pela lista de listas de reprodução em destaque.
* Os campos de filtro da API e seus possíveis valores / tipo devem ser montados ao consumir esta API * [1. Filtros de listas de reprodução] * (http://www.mocky.io/v2/5a25fade2e0000213aa90776)
* As playlists em destaque a serem exibidas devem ser consumidas nesta API * [2. Veja a documentação do Spotify] * (https://developer.spotify.com/web-api/get-list-featured-playlists/)
* Toda vez que o usuário alterar qualquer informação sobre o componente do filtro, a lista deve ser atualizada de acordo. No caso de alteração do campo de filtro da API, você deve lembrar-se da API de listas de reprodução com os parâmetros de filtro todas as vezes.
* Considerando que vivemos em um mundo caótico e em rápida mudança, a página deve atualizar seu conteúdo a cada 30 segundos, para ver se alguma informação das APIs do Spotify foi alterada.

## Sugestões ou restrições

* Usaremos uma API do Spotify Web API. Você deve seguir o guia do Spotify para criar um token necessário para acessar a API do Spotify.
* Para montar os campos de filtro de API no componente de filtro, você * deve * consumir a API que fornece os metadados sobre os campos (Link 1).
* Você pode usar o Material UI, Bootstrap ou qualquer outro kit de ferramentas para acelerar sua resolução. Não forneceremos nenhum protótipo ou design de interface do usuário.

## Requisitos não funcionais

Como esta aplicação será um sucesso mundial, ela deve estar preparada para ser acessível, responsiva, tolerante a falhas e resiliente.
Nós recomendamos fortemente usar o React para criar o aplicativo.
Além disso, explique brevemente os detalhes da arquitetura da sua solução, a escolha de padrões e estruturas.
De um fork neste repositório e envie seu código.